const express = require('express');
const mongoose = require('mongoose');


const app = express();
const port = 4000;

//database information and connection. 
const dbName = "dbZuitt"
const dbUser = "admin"
const dbPass = "admin131"
const connect = `mongodb+srv://${dbUser}:${dbPass}@personaldatabase.gqla2.mongodb.net/${dbName}?retryWrites=true&w=majority`;
const connectStatus = mongoose.connection;
//mongoose connect db and client 
mongoose.connect(connect, { useNewUrlParser: true, useUnifiedTopology: true});
//checks the connection to the database.
connectStatus.on('error', console.error.bind(console, "Error: Failed to connect to the database"));
//connection is successful
connectStatus.once('open', () => console.log("Connected to the cloud database"));
//Database Schema
const taskSchema = new mongoose.Schema({
	
	name: String, 
	status: {
		type: 	String,
		default: 'pending'
	}
})

//Users Schema 
const UserSchema = new mongoose.Schema({
	username: String,
	password: String
})

/* 
	Model
	* they use schemas and they act as the middleman from server to our database. 
	* model can now be used to run commands for interacting with our database. 
	* !important: Naming Convention - Name of model must be capitalized and singular form. ex: User.
*/
const Task = mongoose.model('Task', taskSchema);

//Users Model
const User = mongoose.model('User', UserSchema);

/*
	Business Logic 
	1. Add a functionality to check if there are duplicate tasks.
		- if the task already exists in the db, we return an error. 
		- if the task doesn't exists in the db, we add it in the db. 
	2. The task data will be coming from the request body. 
	3. Create a new Task object with the name field property.
*/

/*
	Users Business Logic 
	1. Add a functionality to check if there are duplicate username.
		- if the user already exists in the database we return an err. 
		- if the user doesn't exists in the database, we add it in the database. 
	2. The user data will be coming from the request's body.
	3. Create a new user object with a username and password fields/properties.
*/
//middlewares 
app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.post('/tasks', (req, res) => {
	/* 
		Insert a specific task in the database and validate if exist.
	*/
	//check any duplicate task. 
	Task.findOne({name: req.body.name}, (err, result) => {
		if(result != null && result.name === req.body.name){
			res.status(404).json(`Error: Duplicate Task Found`);
		}else{
			let newTask = new Task({
				name: req.body.name
			})
			// save method will accept  a callback function which stores any errors in the first parameter and will store the newly saved docs in the second parameter.
			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					res.status(404).json(`saveErr`);
				}else{
					res.status(201).json('New Task Created');
				}
			})
		}
	})
})

// retrieving all task.
app.get('/tasks', (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			res.status(404).json(err);
		}else{
			res.status(200).json({
				data: result
			})
		}
	})
})

//User Create
app.post('/signup', (req, res) => {
	User.find({name: req.body.username}, (err, result) => {
		console.log(result);
		if(result != null && result.username === req.body.username){
			res.status(404).json("Error: Username Already Exists!");
		}else{
			let newUser = new User({
				username: req.body.username,
				password: req.body.password 
			})

			newUser.save((saveErr, saveUser) => {
				if(saveErr){
					res.status(404).json(`saveErr`);
				}else{
					res.status(201).json(`${req.body.username} successfully registered!`)
				}
			})
		}
	})
})

app.get('/user', (req, res) => {
	User.find({}, (err, result) => {
		if(err){
			res.status(404).json(err);
		}else{
			res.status(200).json({
				data: result
			})
		}
	})
})

app.listen(port, function(){
	console.log(`Server is running at Port ${port}`);
})
